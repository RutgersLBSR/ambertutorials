<html>
  <head>
    <title>Absolute Solvation Free Energy using ACES</title>
    <link href="tutorial.css" rel="stylesheet">
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script async="async" id="MathJax-script" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
  </head>
  <body>
    <h1>Absolute Solvation Free Energy using ACES</h1>
    <div class="author">
      <p>Ryan Snyder¹ and Darrin M. York¹</p>
      <p>¹ Laboratory for Biomolecular Simulation Research, Institute for Quantitative Biomedicine and Department of Chemistry and Chemical Biology, Rutgers University, Piscataway, NJ 08854, USA</p>
    </div>
    <h2>Learning Objectives</h2>
    <ul>
      <li>Prepare a system for AFE calculations</li>
      <li>Perform a &quot;Burn-in Simulation&quot;</li>
      <li>Optimize λ-schedule</li>
      <li>Calculate free energy using edgembar</li>
    </ul>
    <h2>Introduction</h2>
    <p>This tutorial will demonstrate how to calculate the absolute solvation free energy of a small molecule using the DDBoost package [
      <a href="https://doi.org/10.1021/acs.jcim.2c00879">Ganguly 2022</a>]. Solvation free energies are fundamental thermodynamic quantities that are related to a number of physical properties, such as pKₐ, that can have broad implications on rational drug design. A thermodynamic cycle (Fig. 1) is used to determine the absloute solvation free energy. 
    </p>
    <div style="text-align: center;">
      <figure>
        <img alt="Figure 1" src="figures/Fig1.png">
        <figcaption>
          <b>Figure 1. </b>The thermodynamic cycle of an absolute solvation free energy calculation
        </figcaption>
      </figure>
    </div>
    <p>This involves the annihilation of a molecule to an ideal gas in each of the gas and condensed phases, that is, transition the molecule from its real state (λ=0) to its dummy state (λ=1), where the molecule is decoupled from its enviornment and the intramolecular interactions are scaled. Because the total free energy of a closed thermodynamic cycle must be zero, the solvation energy is thus determined by: </p>
    <p>
\[
\Delta\mathit{G}_{\mathrm{Solvation}} = \Delta\mathit{G}_{\mathrm{Annihilation}}^{1} - \Delta\mathit{G}_{\mathrm{Annihilation}}^{0}
\]
</p>
    <p>
Acetic acid will be the small molecule used in this tutorial.
Notably, acetic acid establishes an intramolecular hydrogen bonding interaction in the gas phase, thus orienting the carboxylic acid group in a syn conformation (Fig. 2A), that is not present in the aqueous phase, where the carboxylic acid is arranged in an anti conformation (Fig. 2B).
</p>
    <div style="text-align: center;">
      <figure>
        <img alt="Figure 2" src="figures/Fig2.png">
        <figcaption>
          <b>Figure 2. </b>Conformations of acetic acid in the gas phase (A) and the solution phase (B).
        </figcaption>
      </figure>
    </div>
    <p>
Energy barriers between the syn and anti conformation as a function of the (O=C-O-H) torsion angle are ~11.0 and ~6.5 kcal/mol in the gas and solution phases respectively 
[
      <a href="https://doi.org/10.1021/acs.jcim.8b00835">Lim 2019</a>
]. These high conformational barriers may persist even in the dummy state (λ=1) depending on the way the dummy state is defined. The SC transforming separable coordinate/softcore) region for an absolute solvation 
free energy calculation will involve the entire small molecule meaning that the dummy states in both the gas and solution phase are identical.  
If the internal interactions of the acetic acid molecule are not scaled (SC1), the conformational transition in the dummy state will maintain the energy barrier of the the transition of the real state in the gas phase.
Alternatively, if internal electrostatics are scaled to zero (SC2), the dummy state conformational transition will produce an energy barrier more consistent with that of the real state in the solution phase. 
In order to find the correct absolute solvation free energy, we must account for the transition from the syn to the anti conformation and sample the relevant phase space across the transition pathway.
We will do this using the alchemical enhanced sampling (ACES) method. 
The ACES method involves the use of SC25, where the internal LJ interactions, excluding the 1-4 LJ, bond length, and bond angle energy terms are kept in the dummy state. 
This treatment eliminates the energy barrier for the dihedral torsion angle around single bonds; thus enabling an enhanced sampled state. 
The ACES method then uses Hamiltonian replica exchange molecular dynamics (HREMD) to enable further conformational sampling in the real state.

    </p>
    <p>
We will look to replicate the simulations described in the paper: ACES: Optimized Alchemically Enhanced Sampling for the absolute hydration free energy of acetic acid [
      <a href="https://doi.org/10.1021/acs.jctc.2c00697">Lee 2023</a>] with a few modifications. 
This will involve using the general amber force force field, an OPC water box extending 12 Å from the ligand, and 24, linearly spaced alchemical states ranging from the real state a λ = 0.0 to the dummy state at λ = 1.0.
In place of the four, 6 ns long individual trials, this tutorial involves running a single, 1.2 ns simulation that we refer to a &quot;burn-in&quot; simulation. 
Burn-in simulation are used to optimize the λ-schedule, allowing user to reduce the number of lambda windows, therby increasing computational efficiency.

    </p>
    <h2>Initial Setup</h2>
    <p>To begin, let us prepare some directories. First, make a directory called ASFETutorial. Enter this directory and make a directory called &quot;initial&quot;. Inside of this directory, make a new directory called AceticAcid. We will copy files prepared during the &quot;Preparing a Small Molecule from PDB to Simulation&quot; tutorial. Copy acetic_acid.mol2, acetic_acid.frcmod, and acetic_acid.lib to this new directory. Rename each file to have the name acetic_acid_0 followed by the file extension.</p>
    <h2>Prepare for Simulations</h2>
    <p>Now we can use the FE-Workflow tool in the DDBoost package to prepare our system for the necessary simulations. Exit the &quot;initial/AceticAcid&quot; directory. In the ASFETutorial/ make a new direcory called &quot;asfe&quot;. Enter this direcotry directory and place the file &quot;input&quot;, which can be downloaded 
      <a href="files/asfe/input">here</a>. In the event that you are unable to download this file, you should be able to find a generic input file in your FE-Workflow directory. 
    </p>
    <p>Open the input file and ensure that the following key options are set:</p>
    <div class="file">
      <code>path_to_input=../initial<br>system=AceticAcid<br>ticalc=asfe<br>translist=(acetic_acid)<br>pff=ff19SB<br>lff=gaff2<br>wm=opc<br>ionconc=0.15<br>twostate=false<br>ntrials = 1<br>nstlimti=5<br>numexchgti=60000<br>hmr=true<br>gti_add_sc=25</code>
    </div>
    <p>Now run the following command:</p>
    <div class="file">
      <code>setup_fe</code>
    </div>
    <p>The workflow will now spend some time preparing the system and generating all of the input files needed for analysis in the following steps:</p>
    <ol type="1">
      <li>The workflow reads and parses the input file. The variables defined here are used in all other steps. The ticalc variable will dictate how all subsequent steps are preformed, as an ASFE calculation will be prepared differently from a RBFE calculation.</li>
      <li>The workflow generates an initial setup. Using the mol2, frcmod, and lib files in the directory defined by the path_to_input variable, the workflow will perform all of the steps needed to prepare the files needed to run simulations in both the aqueous and gas phases. Durring this process, the workflow will loop over all molecules in the translist variable, build system parameters using the forcefields specified by the pff and lff variables, solvate the system, and add sodium and chloride ions to the concentration defined by the ionconc variable. If the hmr variable is set to true, parmed is used to perform hydrogen mass repartitioning on the hydrogen atoms on the solute.</li>
      <li>The workflow generates folders for each molecule containing the parameters, initial coordinates, input files, and slurm submission scripts. Variables with-in the input file, such as gti_add_sc are used to set variables used by AMBER.</li>
    </ol>
    <h2>Launch Alchemical Simulations</h2>
    <p>
Go to the /path/to/ASFETutorial/asfe/AceticAcid/unified/run/acetic_acid/aq/ directory. 
A file called &quot;run_alltrials.slurm&quot; should have been created. 
This file is a job submission script for a computing cluster using the SLURM scheduler. 
It will need to be modified if you intend to run your program using another scheduler, such as MOAB, or run directly as a shell script.
If one wishes to run the script on SDSC's Expase, the &quot;#SBATCH -A &lt;&lt;project*&gt;&gt;&quot; line must be added. 
Once the necessary modifications have been made, you can run the program.
Users at the SDSC free energy workshop can skip ahead to the &quot;Perform Analysis using edgembar&quot; section and copy the data produced by the following steps.  
For example, if your computing cluster uses the SLURM scheduler:
</p>
    <div class="file">
      <code>sbatch run_alltrials.slurm</code>
    </div>
    <p>This will launch a series of simulations :</p>
    <p>The workflow will now spend some time preparing the system and generating all of the input files needed for analysis in the following steps:</p>
    <ol type="1">
      <li>Beginning with the real end-state, (λ=0), in the aqueous phase, the small molecule will be minimized in two steps. First, a restraint is placed on the solute, and the solvent undergoes energy minimization. Second, the restraint on the solute is removed and energy minimization occurs for the entire system.</li>
      <li>Next, the real end-state system is heated while restraining the solute under constant volume conditions.</li>
      <li>The real end-state system is then equilibrated first with a restrained solute, then without a restrained solute under constant pressure conditions.</li>
      <li>The alchemical states are now &quot;grown-in&quot; by performing energy minimization on the real end-state structure with the λ-dependent Hamiltonian.</li>
      <li>All states are heated with a restraint on solute atoms under constant volume conditions.</li>
      <li>All states are allowed to equilibrate with and then without a solute restraint under constant pressure conditions.</li>
      <li>Using the equilbrated aqueous structures, the solute structures are extracted and equilibrated in the gas phase</li>
      <li>Finally, in each of the aqueous and gas phases, Hamiltonian replica exchange is performed between λ windows. Replica exchange attempts will occur based on the number of MD steps previously defined by the nstlimti variable in the input file. The numexchgti variable will determine the number of exchange attempts made.</li>
    </ol>
    <h2>Perform Analysis using FE-Workflow</h2>
    <p>
Go to the path/to/ASFETutorial/ directory and open the input file using a text editor of your choice.
Set the following options:
</p>
    <div class="file">
      <code>stage=analysis<br>path_to_data=/path/to/ASFETutorial/asfe/AceticAcid/unified/run</code>
    </div>
    <p>if you have experimental data for your small molecule(s), you can enter the data in a file &quot;Expt.dat&quot; in the format:</p>
    <div class="file">
      <code>name_in_translist    expt_value</code>
    </div>
    <p>and set the option: </p>
    <div class="file">
      <code>exptdatafile=Expt.dat</code>
    </div>
    <p>If there is no experimental data available, set the option:</p>
    <div class="file">
      <code>exptdatafile=skip</code>
    </div>
    <p>For acetic acid, the Expt.dat file should look like:</p>
    <div class="file">
      <code>acetic_acid   -6.69</code>
    </div>
    <p>Now, perform the analysis using: </p>
    <div class="file">
      <code>setup_fe</code>
    </div>
    <h2>Perform Analysis using Edgembar</h2>
    <p>
The workflow has been setup to automatically perform analysis using the edgembar tools; however, interested readers may wish to further control analysis by directly calling edgembar tools instead of using the workflow for analysis. 
The first step of edgembar would be to extract the relevant data from the mdout files using edgembar-amber2dats.py.
Currently, the workflow will use the command: 
</p>
    <div class="file">
      <code>edgembar-amber2dats.py -r ${path_out}/../remt1.log --odir=${path_out} $(ls ${path_data}/*ti.mdout)</code>
    </div>
    <p>
Where ${path_data} is the path to the mdout files and ${path_out} is the location where data files should be written.
Use this command for each trial in both the solution and gas phase. 
Other options that could be added to the edgembar-amber2dats.py command would be: 
</p>
    <ul>
      <li>--nmax [int]: By default, edgembar will only analyze 10000 samples from the mdout file. In this tutorial each of the mdout files will contain 60000 samples. Increasing the number of samples will increase the precision of MBAR; however, it will also increase the computational time needed to calculate the free energy</li>
      <li>--exclude : if included, this option will prevent untrustworth samples from being extracted.</li>
    </ul>
    <p>
The edgembar-amber2dats.py command will extract data from the output file in ${path_data}. The files extracted to ${path_out} will fall into two general formats. dvdl_LAM.dat, where LAM is a λ value for which there is an output, contains the partial derivative of the potential energy with respect to λ. This data is used to determine the ASFE by thermodynamic integration (TI). Files will also be produced to follow the efep_LAM1_LAM2.dat format. These files will contain the energy for the conformations produced at λ=LAM1 using a λ=LAM2 value for the Hamiltonian. These files will be used to determine the ASFE using MBAR. 
</p>
    <p>
The extracted data files are available for SDSC users at /expanse/projects/qstore/amber_ws/tutorials/AFE_acetic_acid/results. 
Note that for our own convience, we have renamed results/data/acetic_acid to results/data/acetic_acid_vac~acetic_acid_aq.
Additionally, we are providing data for 4 independent trials of 6 ns so that we can perform analysis on production runs. 
Next, open a text editor and prepare a file called DiscoverEdges.py:    
</p>
    <div class="file">
      <code>#!/usr/bin/env python3</code><br>
      <code>import edgembar</code><br>
      <code>import os</code><br>
      <code>from pathlib import Path</code><br><br>
      <code>odir = Path(&quot;analysis&quot;)</code><br><br>
      <code>s = r&quot;data/{edge}/{env}/{trial}/efep_{traj}_{ene}.dat&quot;</code><br>
      <code>exclusions=None</code><br>
      <code>edges = edgembar.DiscoverEdges(s, exclude_trials=exclusions,</code><br>
      <code>                               target=&quot;aq&quot;,</code><br>
      <code>                               reference=&quot;vac&quot;)</code><br><br>
      <code>if not odir.is_dir():</code><br>
      <code>    os.makedirs(odir)</code><br><br>
      <code>for edge in edges:</code><br>
      <code>    for trial in edge.GetAllTrials():</code><br>
      <code>        trial.reverse()</code><br>
      <code>    fname = odir / (edge.name + &quot;.xml&quot;)</code><br>
      <code>    edge.WriteXml(fname)</code>
    </div>
    <p>
DiscoverEdges.py will be used to write information about the data structure used to store our data. Run this code using: 
</p>
    <div class="file">
      <code>python DiscoverEdges.py</code>
    </div>
    <p>
This step is used to create a set of xml files that describe the directory format used to store the data and the λ values where MD was preformed. Then, use the following commands to calculate the free energy and prepare an html report: 
</p>
    <div class="file">
      <code>OMP_NUM_THREADS=4 edgembar_omp --halves --fwdrev &quot;analysis/acetic_acid_vac~acetic_acid_aq.xml&quot;</code><br>
      <code>edgembar-WriteGraphHtml.py -o analysis/Graph.html $(ls analysis/*~*.py)</code><br>
      <code>cd analysis</code><br>
      <code>python acetic_acid_vac~acetic_acid_aq.py</code>
    </div>
    <p>
During these steps, edgembar uses the previously mentioned dvdl_LAM.dat and efep_LAM1_LAM2.dat files to calculate the ASFE. The data is stored in a set of py files, that are then converted into html files for analysis.  More details on using edgembar can be found at https://rutgerslbsr.gitlab.io/fe-toolkit/edgembar/index.html. 
</p>
    <h2>View Data</h2>
    <p>The data will be written to files in the /path/to/ASFETutorial/asfe/results/analysis/ directory. Here, there should be two .html files: 
      <a href="files/asfe/Graph.html">Graph.html</a> and 
      <a href="files/asfe/acetic_acid_vac~acetic_acid_aq.html">acetic_acid_vac~acetic_acid_aq.html</a>.
    </p>
    <p>You can open these files in a web browser. They should look like this:</p>
    <div style="text-align: center;">
      <figure>
        <img alt="Figure 3" src="figures/Fig3.png">
        <figcaption>
          <b>Figure 3. </b>An Example of Graph.html
        </figcaption>
      </figure>
    </div>
    <h2>Optimizing λ Schedule</h2>
    <p>Now that we've completed our burn-in simulations, we will aim to optimize our λ schedule to maximize efficieny and replica exchange frequency, thereby maximizing the degree of enhanced sampling. [
      <a href="https://doi.org/10.1021/acs.jctc.4c00251">Zhang 2024</a>].
    </p>
    <p>To do this, we will use the fekutils-tischedule.py tool in fe-toolkit.</p>
    <div class="file">
      <code>fetkutils-tischedule.py --opt 16 --ar --ssc --plot opt_ar_16.png  -o opt_ar_16.txt ./data/acetic_acid_vac~acetic_acid_aq/aq/1/</code>
    </div>
    <p>The --opt option allows us to specify the number of λ points we would like to optimize.</p>
    <p>The --ar option means that we will use the predicted HREMD acceptance ratio as our optimization criteria.</p>
    <p>The --ssc option means that we will optimize two parameters - the order of the SSC function for the first half and second half of the λ schedule as opposed to 16 independent variables for each λ position.</p>
    <p>The --plot option is used to generate a graph of the predicted H-REMD acceptance ratios between windows in the optimized λ space (Fig. 4).</p>
    <p>The -o option allows us to write the optimized λ schedule to opt_ar_16.txt</p>
    <p>Finally, '.' is used to tell fekutils-tischedule.py where the data files are located.</p>
    <div style="text-align: center;">
      <figure>
        <img alt="Figure 4" src="figures/Fig4.png">
        <figcaption>
          <b>Figure 4. </b>
          <b>(LEFT)</b>Optimized λ-spacing and predicted H-REMD AR between windows.
          <b>(RIGHT)</b>A Map of the interpolated H-REMD Acceptance Ratios as a function of the λ value for the first λ window (x-axis) and the λ value for the first λ window (y-axis)
        </figcaption>
      </figure>
    </div>
    <p>fetkutils-tischedule.py reads data from the efep_LAM1_LAM2.dat files to determine the average probability of a replica exchange between windows at λ=LAM1 and λ=LAM2. These calculated values are interpolated using a radial basis function to be able to predict the probability of a replica exchange between two windows at any λ value. Smooth-step function polynomials for the first (λ=0-0.5) and second (λ=0.5-1.0) halves of the λ are optimized such that a specified number of lambda windows will have predicted H-REMD acceptance ratios as uniformly as possible.</p>
    <h2>Final Remarks</h2>
    <p>If you use this tutorial, please cite the following papers:</p>
    <ul>
      <li>Variational Method for Networkwide Analysis of Relative Ligand Binding Free Energies with Loop Closure and Experimental Constraints<br>Timothy J. Giese and Darrin M. York<br>J. Chem. Theory Comput. (2021) 17, 1326-1336 (
        <a href="https://doi.org/10.1021/acs.jctc.0c01219">10.1021/acs.jcim.0c00613</a>)
      </li>
      <li>AMBER Drug Discovery Boost Tools: Automated Workflow for Production Free-Energy Simulation Setup and Analysis (ProFESSA)<br>Abir Ganguly, Hsu-Chun Tsai, Mario Fernández-Pendás, Tai-Sung Lee, Timothy J. Giese, and Darrin M. York<br>J. Chem. Inf. Model. (2022) 62, 6069-6083 (
        <a href="https://doi.org/10.1021/acs.jcim.2c00879">10.1021/acs.jcim.2c00879</a>)
      </li>
      <li>ACES: Optimized Alchemically Enhanced Sampling<br>Tai-Sung Lee, Hsu-Chun Tsai, Abir Ganguly, and Darrin M. York<br>J. Chem. Theory Comput. 2023, 19, 2, 472–487 (
        <a href="https://doi.org/10.1021/acs.jctc.2c00697">10.1021/acs.jctc.2c00697</a>)
      </li>
      <li>Alchemical Enhanced Sampling with Optimized Phase Space Overlap<br>Shi Zhang, Timothy J. Giese, Tai-Sung Lee, and Darrin M. York<br>J. Chem. Theory Comput. (2024) 20, 3935-3953 (
        <a href="https://doi.org/10.1021/acs.jctc.4c00251">10.1021/acs.jctc.4c0025</a>)
      </li>
    </ul>
    <h2>References</h2>
    <ul>
      <li>AMBER Drug Discovery Boost Tools: Automated Workflow for Production Free-Energy Simulation Setup and Analysis (ProFESSA)<br>Abir Ganguly, Hsu-Chun Tsai, Mario Fernández-Pendás, Tai-Sung Lee, Timothy J. Giese, and Darrin M. York<br>J. Chem. Inf. Model. (2022) 62, 6069-6083 (
        <a href="https://doi.org/10.1021/acs.jcim.2c00879">10.1021/acs.jcim.2c00879</a>)
      </li>
      <li>Assessing the Conformational Equilibrium of Carboxylic Acid via Quantum Mechanical and Molecular Dynamics Studies on Acetic Acid<br>Victoria T. Lim, Christopher I. Bayly, Laszlo Fusti-Molnar, and David L. Mobley<br>J. Chem. Inf. Model. 2019, 59, 5, 1957–1964 (
        <a href="https://doi.org/10.1021/acs.jcim.8b00835">10.1021/acs.jcim.8b00835</a>)
      </li>
      <li>ACES: Optimized Alchemically Enhanced Sampling<br>Tai-Sung Lee, Hsu-Chun Tsai, Abir Ganguly, and Darrin M. York<br>J. Chem. Theory Comput. 2023, 19, 2, 472–487 (
        <a href="https://doi.org/10.1021/acs.jctc.2c00697">10.1021/acs.jctc.2c00697</a>)
      </li>
      <li>FreeSolv: a database of experimental and calculated hydration free energies, with input files<br>David L. Mobley and J. Peter Guthrie<br>J. Comput. Aided Mol. Des. (2014) 28, 711–720 (
        <a href="https://doi.org/10.1007/s10822-014-9747-x">10.1007/s10822-014-9747-x</a>)
      </li>
      <li>Alchemical Enhanced Sampling with Optimized Phase Space Overlap<br>Shi Zhang, Timothy J. Giese, Tai-Sung Lee, and Darrin M. York<br>J. Chem. Theory Comput. (2024) 20, 3935-3953 (
        <a href="https://doi.org/10.1021/acs.jctc.4c00251">10.1021/acs.jctc.4c0025</a>)
      </li>
    </ul>
  </body>
</html>
