<html>
  <head>
    <link rel="stylesheet" href="tutorial.css">
    
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>

    <script>
window.MathJax = {
  tex: {
    tags: 'ams'
  }
};
    </script>
    

    <style>
      ul {
          padding-left: 20px;
          list-style-position: outside;
      }
      li {
          padding-bottom: 0.5em;
      }
    </style>
    
  </head>
  <body>
    
    <h1>
      MM-to-QM/MM Free Energy "Bookend" Corrections using
      GFN2-xTB and QD&pi;2 machine learning potential.
    </h1>
    
    <h2 id="Objectives"> Learning objectives </h2>
    <p>
      <ul>
        <li>
          <a> Learn how to use sander.MPI to calculate MM-to-QM/MM free energy corrections using the reference potential method</a>
        </li>
        <li>
          <a> Learn to use edgembar to analyze the free energy from the sander output files </a>
        </li>
        <li>
          <a> Learn to apply a pre-trained DeePMD-kit machine learning correction potential</a>
        </li>
	
      </ul>
    </p>
    
    
    <h2 id="Literature"> Relevant literature </h2>
    <p>
      <ul>
	<li>
	  A recent article discussing the calculation of free energies using an indirect approach from a reference potential, <a href="https://pubs.acs.org/doi/10.1021/acs.jctc.9b00401">Giese (2019)</a>.
	</li>
        <li>
	  Some of the first works to develop the reference potential approach include
          <a href="https://pubs.acs.org/doi/10.1021/j100181a009">Gao (1992)</a>
	  and
	  <a href="https://onlinelibrary.wiley.com/doi/10.1002/jcc.540130212">Luzhkov (1992)</a>.
        </li>
	<li>
	  Implementation of GFN2-xTB and DeePMD-kit into sander,
	  <a href="https://pubs.acs.org/doi/10.1021/acs.jpcb.4c01466">Giese (2024)</a>.
	</li>
        <li>
          <a href="https://docs.deepmodeling.com/projects/deepmd/en/r2/">DeePMD-kit documentation</a>
        </li>
      </ul>
    </p>
    
    
    <h2 id="Introduction"> Introduction </h2>
    <p>
      This tutorial will demonstrate how to calculate the absolute
      solvation free energy of a small molecule using a QM/MM potential
      energy function.  Specifically, we shall estimate the solvation
      free energy of acetic acid using the GFN2-xTB semiempirical
      model supplemented with a QD&pi;2 machine learning potential.
      The method of calculation is based on the use of an indirect
      thermodynamic cycle, illustrated in <a href="#Figure1">Figure 1</a>.

      
      <div class="figure" id="Figure1">
	<img src="imgs/ThermoCycle.png" style="display: block; margin-left: auto; margin-right: auto; width: 300px;"/>
	<div class="caption">
	  <span class="label">Figure 1</span>. An indirect thermodynamic cycle for calculating the a QM/MM solvation free energy using a MM reference potential. The vertical arrows are the so-called "bookend" corrections. They are the free energy difference between the MM and QM/MM potential energy functions.
	</div>
      </div>


    </p>

    <p>
      
      It is impractical to
      directly estimate the solvation free energy using a QM/MM potential
      [line 1 of Eq. \eqref{E:dAQMMM}]
      due to its high cost and the large amount of required sampling.     
      
      
      \begin{equation}\label{E:dAQMMM}
      \begin{split}
      \Delta A_{\text{QM/MM},\text{solv}}
      =&
      A_{\text{QM/MM},\text{aq}} - A_{\text{QM/MM},\text{gas}}
      \\
      =&
      \Delta A_{\text{MM},\text{solv}}
      +
      \Delta A_{\text{MM}\rightarrow\text{QM/MM},\text{aq}}
      -
      \Delta A_{\text{MM}\rightarrow\text{QM/MM},\text{gas}}
      \end{split}
      \end{equation}


      In contrast, a MM force field is sufficiently affordable to
      obtain reasonable estimates of &Delta;A<sub>MM,solv</sub>
      [Eq. \eqref{E:dAMM}].

      
      \begin{equation}\label{E:dAMM}
      \Delta A_{\text{MM},\text{solv}} = A_{\text{MM},\text{aq}} - A_{\text{MM},\text{gas}}
      \end{equation}


      Because the free energy is a state function, the sum of
      free energies along any closed path is zero.
      This is mathematically expressed by the second line
      in Eq. \eqref{E:dAQMMM}.
      In other words,
      the indirect thermodynamic approach calculates
      &Delta;A<sub>QM/MM,solv</sub>
      from a MM reference potential by including "bookend corrections"
      [Eqs. \eqref{E:BEaq}-\eqref{E:BEgas}].
      
      \begin{equation}\label{E:BEaq}
      \Delta A_{\text{MM}\rightarrow\text{QM/MM},\text{aq}}
      =
      A_{\text{QM/MM},\text{aq}} - A_{\text{MM},\text{aq}}
      \end{equation}

      \begin{equation}\label{E:BEgas}
      \Delta A_{\text{MM}\rightarrow\text{QM/MM},\text{gas}}
      =
      A_{\text{QM/MM},\text{gas}} - A_{\text{MM},\text{gas}}
      \end{equation}

      The bookend corrections often do not require extensive
      sampling because it does not involve a physical
      transformation of the system.
      
    </p>


    
    
    <p>
      A bookend correction can be calculated from the
      <a href="https://en.wikipedia.org/wiki/Thermodynamic_integration">
	thermodynamic integration</a>
      (TI) [Eq. \eqref{E:TI}] or
      <a href="https://en.wikipedia.org/wiki/Bennett_acceptance_ratio">
	multistate Bennett acceptance ratio method</a> (MBAR)
      by analyzing the equilibrium sampling produced
      by a &lambda;-dependent potential energy function
      that transforms between MM and QM/MM potentials [Eq. \eqref{E:Ulam}].
      
      \begin{equation}\label{E:TI}
      \Delta A_{\text{MM}\rightarrow\text{QM/MM}}
      =
      \int_{0}^{1} \left\langle \frac{\partial U}{\partial \lambda} \right\rangle_{\lambda} d\lambda
      \end{equation}
      
      
      \begin{equation}\label{E:Ulam}
      U(\mathbf{r};\lambda) = \lambda U_{\text{QM/MM}}(\mathbf{r}) + (1-\lambda) U_{\text{MM}}(\mathbf{r})
      \end{equation}


      \begin{equation}\label{E:dUdlam}
      \frac{\partial U(\mathbf{r};\lambda)}{\partial\lambda} = U_{\text{QM/MM}}(\mathbf{r}) - U_{\text{MM}}(\mathbf{r})
      \end{equation}

      
    </p>


    <p>
      "Multisander" (i.e., the MPI parallel version of sander, sander.MPI)
      evaluates Eq. \eqref{E:Ulam} using a "groupfile" and a few key options
      in the &amp;cntrl; namelist.
      The first line of the groupfile defines the MM potential energy
      (the &lambda;=0 state),
      and the second defines the QM/MM potential energy
      (the &lambda;=1 state).
      An example is shown in <a href="#Figure2">Figure 2</a>.

      
      <div class="figure" id="Figure2">
	
	<div class="file">
	  <code>
	  -O -p ../../../common/aq.parm7 -i mm.0.00.mdin -o mm.0.00.mdout -c init.0.00.rst7 -r mm.0.00.rst7 -x mm.0.00.nc -inf mm.0.00.mdinfo<br>
-O -p ../../../common/aq.parm7 -i qm.0.00.mdin -o qm.0.00.mdout -c init.0.00.rst7 -r qm.0.00.rst7 -x qm.0.00.nc -inf qm.0.00.mdinfo
	  </code>
	</div>
	
      	<div class="caption">
	  <span class="label">Figure 2</span>.
	  An example groupfile that defines a &lambda;-dependent
	  potential energy function.
	  There must be exactly 2 lines.
	  The first line is the
	  &lambda;=0 state. The second line is the &lambda;=1 state.
	  Both systems must read the same input coordinates.
	  The input files should generally be the same, except
	  for the values of ifqnt=0 or 1 (which turns on/off the QM/MM
	  potential) and idprc=0 or 1 (which turns on/off the DeePMD-kit
	  machine learning correction).
	  A separate groupfile is needed for each value of &lambda;;
	  in this example, the "0.00" denotes that this groupfile is
	  used to simulate the &lambda;=0 state.
	</div>
      </div>

    </p>

    

    <p>
      The MM and QM/MM input files must be the exact same <i>except</i>
      for the activation/deactivation of the QM (and machine learning)
      potentials.
      The mdin file &amp;cntrl; section must specify the following options:
    </p>
    <ul>
      <li>
	<span style="margin-right: 1em;"><code>icfe=1</code></span>
	This causes the groupfile to be interpretted as
	defining a &lambda;-dependent potential energy function.
      </li>
      <li>
	<span style="margin-right: 1em;"><code>clambda=0.0</code></span>
	This is the value of &lambda; (a floating point number between 0 and 1).
	The &lambda;=0 state corresonds to the first line in the groupfile.
	Both the MM and QM/MM input files should specify the same clambda value.
      </li>
    </ul>
    <p>
      The following is a list of options that may be different in the
      MM and QM/MM input files.
    </p>
    <ul>
      <li>
	<span style="margin-right: 1em;"><code>ifqnt=0</code></span>
	The value of this option (appearing in the &amp;cntrl; section)
	controls whether a QM/MM potential is applied.
      </li>
      <li>
	<span style="margin-right: 1em;"><code>idprc=0</code></span>
	The value of this option (appearing in the optional &amp;dprc; section)
	controls whether a DeePMD-kit machine learning correction is applied.
	The &amp;dprc; section is only applicable if amber was configured
	with interface support to the DeePMD-kit library.
      </li>
      <li>
	<span style="margin-right: 1em;"><code>qm_ewald=0</code></span>
	The value of this option (appearing in the &amp;qmmm; section)
	controls whether periodic electrostatics are included in the
	QM calculation. One normally sets qm_ewald=0; however, when
	calculating a bookend calculation in the gas phase, one can
	turn off the periodic electrostatics.
      </li>
    </ul>
    <p>
      Example MM and QM/MM inputs for the calculation of
      Eq. \eqref{E:BEaq} are shown in <a href="#Figure3">Figure 3</a>.


      <div class="figure" style="min-width: 50em;" id="Figure3">
	
	<div class="file" style="display: inline-block; width: 45%; margin: 0; min-width: 22em;" >
	  <code>
<b>Title: MM input files</b><br>
&cntrl<br>
! IO =======================================<br>
      irest = 1    ! 0 = start, 1 = restart<br>
        ntx = 5    ! 1 = start, 5 = restart<br>
       ntxo = 1    ! rst is formatted file<br>
      iwrap = 1    ! wrap crds to unit cell<br>
     ioutfm = 1    ! write mdcrd as netcdf<br>
       ntpr = 10   ! mdout print freq<br>
       ntwx = 0    ! mdcrd print freq<br>
       ntwr = 500  ! rst print freq<br>
       ntwv = 0<br>
! DYNAMICS =================================<br>
       imin = 0    ! Run dynamics<br>
         dt = 0.002! ps/step<br>
     nstlim = 2500 ! number of time steps<br>
        ntb = 1    ! 1=periodic box<br>
! TEMPERATURE ==============================<br>
      temp0 = 298  ! target temp<br>
   gamma_ln = 5.0  ! Langevin collision freq<br>
        ntt = 3    ! thermostat (3=Langevin)<br>
! PRESSURE  ================================<br>
        ntp = 0    !0=off 1=isotropic scaling<br>
! SHAKE ====================================<br>
        ntc = 2<br>
        ntf = 1<br>
noshakemask=':1'<br>
! MISC =====================================<br>
        cut = 10<br>
         ig = -1<br>
      <span style="background-color: orange;">ifqnt = 0</span><br>
! TI =======================================<br>
       icfe = 1   ! FE simulation<br>
    <span style="background-color: lightgreen;">clambda = 0.0</span> ! lambda. 0=MM, 1=QM/MM<br>
     ifmbar = 1   ! print mbar energies<br>
bar_intervall= 10 ! print freq<br>
bar_l_min   = 0<br>
bar_l_max   = 1<br>
bar_l_incr  = 1<br>
     dynlmb = 0 <br>
      ntave = 0<br>
/<br>
<br>
&qmmm<br>
    qm_theory   = 'XTB'<br>
        qmmask  = ':1'<br>
      qmcharge  = 0<br>
          spin  = 1<br>
       qmshake  = 0<br>
      qm_ewald  = 1<br>
   qmmm_switch  = 1<br>
       scfconv  = 1.e-10<br>
     verbosity  = 0<br>
  tight_p_conv  = 1<br>
  diag_routine  = 0<br>
   pseudo_diag  = 1<br>
  dftb_maxiter  = 100<br>
/<br>
<br>
&xtb<br>
  qm_level="GFN2-xTB"<br>
/<br>
<br>
&dprc<br>
  <span style="background-color: orange;">idprc=0</span><br>
  mask=":1"<br>
  rcut=0.0<br>
  intrafile(1) = "../../../common/qdpi2.pb"<br>
/<br>
	  </code>
	</div>
	
	
	<div class="file" style="display: inline-block; width: 45%; margin: 0; min-width: 22em;" >
	  <code>
<b>Title: QM/MM+MLP input file</b><br>
&cntrl<br>
! IO =======================================<br>
      irest = 1    ! 0 = start, 1 = restart<br>
        ntx = 5    ! 1 = start, 5 = restart<br>
       ntxo = 1    ! rst is formatted file<br>
      iwrap = 1    ! wrap crds to unit cell<br>
     ioutfm = 1    ! write mdcrd as netcdf<br>
       ntpr = 10   ! mdout print freq<br>
       ntwx = 0    ! mdcrd print freq<br>
       ntwr = 500  ! rst print freq<br>
       ntwv = 0<br>
! DYNAMICS =================================<br>
       imin = 0    ! Run dynamics<br>
         dt = 0.002! ps/step<br>
     nstlim = 2500 ! number of time steps<br>
        ntb = 1    ! 1=periodic box<br>
! TEMPERATURE ==============================<br>
      temp0 = 298  ! target temp<br>
   gamma_ln = 5.0  ! Langevin collision freq<br>
        ntt = 3    ! thermostat (3=Langevin)<br>
! PRESSURE  ================================<br>
        ntp = 0    !0=off 1=isotropic scaling<br>
! SHAKE ====================================<br>
        ntc = 2<br>
        ntf = 1<br>
noshakemask=':1'<br>
! MISC =====================================<br>
        cut = 10<br>
         ig = -1<br>
      <span style="background-color: orange;">ifqnt = 1</span><br>
! TI =======================================<br>
       icfe = 1   ! FE simulation<br>
    <span style="background-color: lightgreen;">clambda = 0.0</span> ! lambda. 0=MM, 1=QM/MM<br>
     ifmbar = 1   ! print mbar energies<br>
bar_intervall= 10 ! print freq<br>
bar_l_min   = 0<br>
bar_l_max   = 1<br>
bar_l_incr  = 1<br>
     dynlmb = 0 <br>
      ntave = 0<br>
/<br>
<br>
&qmmm<br>
    qm_theory   = 'XTB'<br>
        qmmask  = ':1'<br>
      qmcharge  = 0<br>
          spin  = 1<br>
       qmshake  = 0<br>
      qm_ewald  = 1<br>
   qmmm_switch  = 1<br>
       scfconv  = 1.e-10<br>
     verbosity  = 0<br>
  tight_p_conv  = 1<br>
  diag_routine  = 0<br>
   pseudo_diag  = 1<br>
  dftb_maxiter  = 100<br>
/<br>
<br>
&xtb<br>
  qm_level="GFN2-xTB"<br>
/<br>
<br>
&dprc<br>
  <span style="background-color: orange;">idprc=1</span><br>
  mask=":1"<br>
  rcut=0.0<br>
  intrafile(1) = "../../../common/qdpi2.pb"<br>
/<br>
	  </code>
	</div>
	
	
      	<div class="caption">
	  <span class="label">Figure 3</span>.
	  Example MM (left) and QM/MM (right) mdin files.
	  The orange color highlights options that may differ
	  between the two files.
	  The green color highlights options that change for
	  each &lambda; state.
	  This particular example is for the &lambda;=0 state.
	  When a machine learning potential is not employed,
	  the &amp;dprc; section can be completely ignored.
	  The <code>idprc=1</code>
	  turns on the machine learning correction, whose
	  parameters are read from the file listed in
	  <code>intrafile(1)</code>.
	  The machine learning potential is applied to the
	  atoms in <code>mask</code>, which is the selected
	  QM atoms.  The QD&pi;2 model corrects the intra-molecular
	  interactions (not the QM/MM interactions); therefore,
	  <code>rcut=0.0</code>.
	</div>
      </div>
      
    </p>

    
    
    <h2 id="Running"> Running the simulations </h2>
    <p>
      Because the potential energy is described using a groupfile,
      one needs the MPI version of sander and at least 2 CPU cores.
      On a slurm-based scheduling system, one can launch 2 (MPI) tasks
      on 1 node using 1 core per task with the following command, which
      one would write in their submission slurm script.
      The <code>-ng 2</code> tells sander to expect to read 2 lines
      from the groupfile.
      <div class="file">
	<code>
	srun -N 1 -n 2 -c 1 --mpi=pmi2 sander.MPI -ng 2 -groupfile mm2qmmm.0.00.groupfile
	</code>
      </div>
    </p>
    <p>
      For accurate results, one should simulate several &lambda; values, such
      as 0.0, 0.2, 0.4, 0.6, 0.8, and 1.0. The bookend transformations tend
      to require sampling of far fewer &lambda;-states than alchemical
      transformations because the system is not undergoing a physical change.
      The free energy calculated from TI or MBAR requires equilibrium
      statistics; therefore, one should equilibrate each
      simulation before gathering production statistics.
    </p>
    
    <h2 id="Example"> Example </h2>
    <p>
      <ul>
	<li>
	  Download <a href="example_mm2qmmm.inputs.tar.gz">example_mm2qmmm.inputs.tar.gz</a>.
      
          The package includes files used to calculate
	  Eqs. \eqref{E:BEaq} and \eqref{E:BEgas} for acetic acid
	  in which a MM potential is transformed to GFN2-xTB QM/MM
	  with QD&pi;2 machine learning corrections.
	  The contents of the files are
	  essentially those shown in Figures
	  <a href="#Figure2">2</a> and <a href="#Figure3">3</a>.
	</li>

	<li>
	  You can unpack the files by typing:
	  <code>tar -xzf example_mm2qmmm.inputs.tar.gz</code>.
      
	  This will create a directory <code>./example_mm2qmmm/</code>.


	  <div class="figure" id="Figure4">

	    
	    <ul style="padding-bottom: 0; margin-bottom: 0;">
	      <li style="padding-bottom: 0; margin-bottom: 0;">
		example_mm2qmmm/
		
		<ul style="padding-bottom: 0; margin-bottom: 0;">
		  <li style="padding-bottom: 0; margin-bottom: 0;">
		    RunExample.sh
		  </li>
		  <li style="padding-bottom: 0; margin-bottom: 0;">
		    MakeTemplateFiles.sh
		  </li>
		  <li style="padding-bottom: 0; margin-bottom: 0;">
		    MakeEquilAndProdDirs.sh
		  </li>
		  <li style="padding-bottom: 0; margin-bottom: 0;">
		    edgembar-bookend2dats.py
		  </li>
		  
		  <li style="padding-bottom: 0; margin-bottom: 0;">
		    init/
		    <ul style="padding-bottom: 0; margin-bottom: 0;">
		      <li style="padding-bottom: 0; margin-bottom: 0;">
			mm.aq.*.rst7
		      </li>
		      <li style="padding-bottom: 0; margin-bottom: 0;">
			mm.vac.*.rst7
		      </li>
		    </ul>
		  </li>
		  
		  <li style="padding-bottom: 0; margin-bottom: 0;">
		    common/
		    <ul style="padding-bottom: 0; margin-bottom: 0;">
		      <li style="padding-bottom: 0; margin-bottom: 0;">
			vac.parm7
		      </li>
		      <li style="padding-bottom: 0; margin-bottom: 0;">
			aq.parm7
		      </li>
		      <li style="padding-bottom: 0; margin-bottom: 0;">
			qdpi2.pb
		      </li>
		    </ul>
		  </li>
		  
		</ul>
		
	      </li>
	    </ul>
	    
	    <div class="caption">
	      <span class="label">Figure 4</span>.
	      Description of the directory structure 
	      contained within
	      <a href="example_mm2qmmm.inputs.tar.gz">example_mm2qmmm.inputs.tar.gz</a>.
	    </div>
	  </div>
	  
	  
	</li>
      
	<li>
	  Change directory to <code>./example_mm2qmmm/</code>
	  and type <code>bash RunExample.sh</code>.
      
	  The <code>RunExample.sh</code> script will prepare a series of
	  groupfiles and corresponding MM and QM/MM input files.
	  These files are organized into several subdirectories, named:
	  prod01, prod02, prod03, prod04.
	  These are 4 directories are used to perform independent trials
	  that depart from structures extracted from
	  MM simulations.
	  
	  The trial directories will contain 2 subdirectories:
	  <code>aq/</code> and <code>vac</code> which contain simulations
	  for Eqs. \eqref{E:BEaq} and \eqref{E:BEgas}, respectively.
	  The sampling within each environment is separated into two directories:
	  <code>equil/</code> and <code>prod/</code>,
	  which are used to perform equilibration and production sampling,
	  respectively.
	  
	  <div class="figure" id="Figure5">

	    
	    <ul style="padding-bottom: 0; margin-bottom: 0;">
	      <li style="padding-bottom: 0; margin-bottom: 0;">
		example_mm2qmmm/
		
		<ul style="padding-bottom: 0; margin-bottom: 0;">
		  
		  <li style="padding-bottom: 0; margin-bottom: 0;">
		    <span style="background-color: orange;">sub.sims.slurm</span>
		  </li>
		  <li style="padding-bottom: 0; margin-bottom: 0;">
		    <span style="background-color: orange;">sub.ana.slurm</span>
		  </li>
		  <li style="padding-bottom: 0; margin-bottom: 0;">
		    RunExample.sh
		  </li>
		  <li style="padding-bottom: 0; margin-bottom: 0;">
		    MakeTemplateFiles.sh
		  </li>
		  <li style="padding-bottom: 0; margin-bottom: 0;">
		    MakeEquilAndProdDirs.sh
		  </li>
		  <li style="padding-bottom: 0; margin-bottom: 0;">
		    edgembar-bookend2dats.py
		  </li>
		  
		  <li style="padding-bottom: 0; margin-bottom: 0;">
		    init/
		    <ul style="padding-bottom: 0; margin-bottom: 0;">
		      <li style="padding-bottom: 0; margin-bottom: 0;">
			mm.aq.*.rst7
		      </li>
		      <li style="padding-bottom: 0; margin-bottom: 0;">
			mm.vac.*.rst7
		      </li>
		    </ul>
		  </li>
		  
		  <li style="padding-bottom: 0; margin-bottom: 0;">
		    common/
		    <ul style="padding-bottom: 0; margin-bottom: 0;">
		      <li style="padding-bottom: 0; margin-bottom: 0;">
			vac.parm7
		      </li>
		      <li style="padding-bottom: 0; margin-bottom: 0;">
			aq.parm7
		      </li>
		      <li style="padding-bottom: 0; margin-bottom: 0;">
			qdpi2.pb
		      </li>
		    </ul>
		  </li>

		  
		  
		  <li style="padding-bottom: 0; margin-bottom: 0; background-color: orange;">
		    <code>prod01/</code><br>
		    <code>prod02/</code><br>
		    <code>prod03/</code><br>
		    <code>prod04/</code><br>
		    <code>template/</code><br>
		    
		    <ul style="padding-bottom: 0; margin-bottom: 0;">
		      <li style="padding-bottom: 0; margin-bottom: 0;">
			<code>aq/</code> 
			<br>
			<code>vac/</code> 
			
			
			<ul style="padding-bottom: 0; margin-bottom: 0;">
			  <li style="padding-bottom: 0; margin-bottom: 0;">
			    <code>equil/</code>
			    <br>
			    <code>prod/</code>
			    
			    <ul style="padding-bottom: 0; margin-bottom: 0;">
			      <li style="padding-bottom: 0; margin-bottom: 0;">
				<code>mm2qmmm.*.groupfile</code>
				<br>
				<code>mm.*.mdin</code>
				<br>
				<code>qm.*.mdin</code>
			      </li>
			    </ul>
			    
			  </li>
			</ul>
			
		      </li>
		    </ul>
		    
		  </li>
		</ul>
	      </li>
	    </ul>
	    
	    <div class="caption">
	      <span class="label">Figure 5</span>.
	      Description of the directory structure after running the
	      <code>RunExample.sh</code> script.
	      The highlighted files and directories are created by the script.
	      The prod01, prod02, prod03, prod04 are 4 independent trials
	      starting from different snapshots extracted from
	      previous MM simulations.  These directories are copies of the
	      template directory.
	    </div>
	  </div>
	</li>

	<li>
	  The <code>RunExample.sh</code> script will prepare a sub.sims.slurm
	  script. The submission script uses slurm arrays to perform the
	  sampling in each trial directory. In other words, the 4 array values
	  (<code>#SBATCH --array=1-4</code>) perform sampling in each
	  of the 4 trial directories.
	</li>


	<li>
	  After the simulations complete, the data can be extracted from the
	  information stored in the files: <code>prod*/*/prod/mm.*.mdout</code>.
	  For your convenience, you can download the necessary output files
	  <a href="example_mm2qmmm.outputs.tar.gz">example_mm2qmmm.outputs.tar.gz</a>.
	  Unpacking the files with <code>tar -xzf example_mm2qmmm.outputs.tar.gz</code>
	  will cause the files to be copied into the subfolder <code>./example_mm2qmmm</code>.
	  That is, unpack example_mm2qmmm.outputs.tar.gz in the same directory you unpacked
	  example_mm2qmmm.inputs.tar.gz

	  <div class="figure" id="Figure6">
	    
	    <ul style="padding-bottom: 0; margin-bottom: 0;">
	      <li style="padding-bottom: 0; margin-bottom: 0;">
		example_mm2qmmm/
		
		<ul style="padding-bottom: 0; margin-bottom: 0;">
		  <li style="padding-bottom: 0; margin-bottom: 0;">
		    <code>analysis/</code><br>
		  </li>
		</ul>
		    
		<ul style="padding-bottom: 0; margin-bottom: 0;">
		  <li style="padding-bottom: 0; margin-bottom: 0;">
		    <code>prod01/</code><br>
		    <code>prod02/</code><br>
		    <code>prod03/</code><br>
		    <code>prod04/</code>
		    
		    <ul style="padding-bottom: 0; margin-bottom: 0;">
		      <li style="padding-bottom: 0; margin-bottom: 0;">
			<code>aq/</code> 
			<br>
			<code>vac/</code> 
			
			
			<ul style="padding-bottom: 0; margin-bottom: 0;">
			  <li style="padding-bottom: 0; margin-bottom: 0;">
			    <code>equil/</code>
			    <br>
			    <code>prod/</code>
			    
			    <ul style="padding-bottom: 0; margin-bottom: 0;">
			      <li style="padding-bottom: 0; margin-bottom: 0;">
				<code>mm.*.mdout</code>
			      </li>
			    </ul>
			
			  </li>
			</ul>
			
		      </li>
		    </ul>
		    
		  </li>
		</ul>
		
	      </li>
	    </ul>	
	    
	    <div class="caption">
	      <span class="label">Figure 6</span>.
	      Description of the directory structure contained within
	      <a href="example_mm2qmmm.outputs.tar.gz">example_mm2qmmm.outputs.tar.gz</a>.
	    </div>
	  </div>
	  
	</li>
	
      </ul>
      
     
    </p>



    <h2 id="Example"> Analysis </h2>
    <p>
      The MM and QM/MM mdout files contain the same potential energy
      and the &part;U/&part;&lambda; data; therefore, we will only look at the MM mdout
      files.
      To perform TI analysis, one must extract the &part;U/&part;&lambda; time series
      from the simulations of each &lambda;-state in each environment (aq or vac)
      and trial (prod01, prod02, prod03, prod04).
      Similarly, MBAR analysis requires the potential energy of each state,
      U(<b>r</b>;&lambda;&apos;).
      Because this is a linear transformation, the potential energy of any intermediate
      state can be computed from the &lambda;=0 and &lambda;=1 states [Eq. \eqref{E:Ulam}].
      Furthermore, the MBAR analysis only requires potential energy differences,
      so we will obtain the same free energy if -- for each frame, <i>k</i> --
      we define the zero of energy to be U<sub>MM</sub>(<b>r</b><sub>k</sub>).
      In which case, the potential energy of state &lambda;&apos;
      evaluated at frame <i>k</i> within the ensemble of state &lambda; is
      given by Eq. \eqref{E:UMBAR}.

      \begin{equation}\label{E:UMBAR}
      \begin{split}
      U(\mathbf{r}_{\lambda k};\lambda^{\prime})
      -
      U_{\text{MM}}(\mathbf{r}_{\lambda k})
       = &
       \lambda^{\prime}
      \left[ U_{\text{QM/MM}}(\mathbf{r}_{\lambda k})-U_{\text{MM}}(\mathbf{r}_{\lambda k}) \right]
      \\
      =&
      \lambda^{\prime}
      \frac{\partial U(\mathbf{r}_{\lambda k};\lambda)}{\partial \lambda}
      \end{split}
      \end{equation}


      The mdout files contain lines that look like:
      <div class="file" style="padding:0; margin-bottom:0.5em;">
	<pre style="margin-top:0.75em; margin-bottom: 0.25em;"><code> NSTEP =       50   TIME(PS) =    1020.100  TEMP(K) =   298.61  PRESS =     0.0
 Etot   =    -61089.2946  EKtot   =      5235.5357  EPtot      =    -66324.8304
 BOND   =         2.8694  ANGLE   =         4.6157  DIHED      =        17.6568
 1-4 NB =         0.4164  1-4 EEL =       -89.3041  VDWAALS    =      5313.5873
 EELEC  =    -42803.8431  EHBOND  =         0.0000  RESTRAINT  =         0.0000
 DV/DL  =   -143769.6429
 Ewald error estimate:   0.2471E-02
 ------------------------------------------------------------------------------</code></pre>
      </div>

      This example shows a sample within <code>example_mm2qmmm/prod01/aq/prod/mm.0.20.mdout</code>.
      That is, it is sample from the &lambda;=0.2 ensemble within the first trial performed in solution.
      The &part;U/&part;&lambda; value is -143769.6429 kcal/mol (the number after <code>DV/DL</code>).
    </p>
    <p>
      The <code>example_mm2qmmm/sub.ana.slurm</code> slurm submission file uses
      the
      <code>example_mm2qmmm/edgembar-bookend2dats.py</code>
      script to extract the relevant data from the mdout files and use edgembar
      to perform the analysis.
      The "bookend-to-dats" script takes a list of mdout filenames
      (e.g. prod01/aq/prod/mm.*.mdout)
      extracts the DV/DL data from each file,
      and writes the TI and MBAR timeseries in a format
      that can be analyzed using the edgembar program.
      The <code>MakeAnalysis.sh</code> script
      executes the script on the mdout files
      from each trial.
      The extracted outputs are written to a series of files:
      <code>analysis/mm~qmmm/<i>env</i>/<i>trial</i>/dats/efep_<i>tlam</i>_<i>elam</i>.dat</code>
      and
      <code>analysis/mm~qmmm/<i>env</i>/<i>trial</i>/dats/dvdl_<i>tlam</i>.dat</code>,
      where <i>env</i> is either aq or vac,
      <i>trial</i> is either t01, t02, t03, or t04,
      <i>tlam</i> is the lambda value used to simulate the trajectory,
      and <i>elam</i> is the lambda value corresponding to the energy within the file.
    </p>

    <p>
      The <code>sub.ana.slurm</code> script
      then creates an XML input file to run edgembar.
      The XML file is provided as <code>mm2qmmm/analysis/xml/mm~qmmm.xml</code>,
      and it is shown in <a href="#Figure7">Figure 7</a>.
      
      
      <div class="figure" id="Figure7" style="min-width: 35em;">
	
	<pre class="file"><code>&lt;?xml version="1.0" ?&gt;
&lt;edge name="mm~qmmm"&gt;
	&lt;env name="target"&gt;
		&lt;stage name="STAGE"&gt;
			&lt;trial name="t01"&gt;
				&lt;dir&gt;mm~qmmm/aq/t01/dats&lt;/dir&gt;
				&lt;ene&gt;0.00000000&lt;/ene&gt;
				&lt;ene&gt;0.20000000&lt;/ene&gt;
				&lt;ene&gt;0.40000000&lt;/ene&gt;
				&lt;ene&gt;0.60000000&lt;/ene&gt;
				&lt;ene&gt;0.80000000&lt;/ene&gt;
				&lt;ene&gt;1.00000000&lt;/ene&gt;
			&lt;/trial&gt;
			&lt;trial name="t02"&gt;
				&lt;dir&gt;mm~qmmm/aq/t02/dats&lt;/dir&gt;
				&lt;ene&gt;0.00000000&lt;/ene&gt;
				&lt;ene&gt;0.20000000&lt;/ene&gt;
				&lt;ene&gt;0.40000000&lt;/ene&gt;
				&lt;ene&gt;0.60000000&lt;/ene&gt;
				&lt;ene&gt;0.80000000&lt;/ene&gt;
				&lt;ene&gt;1.00000000&lt;/ene&gt;
			&lt;/trial&gt;
			&lt;trial name="t03"&gt;
				&lt;dir&gt;mm~qmmm/aq/t03/dats&lt;/dir&gt;
				&lt;ene&gt;0.00000000&lt;/ene&gt;
				&lt;ene&gt;0.20000000&lt;/ene&gt;
				&lt;ene&gt;0.40000000&lt;/ene&gt;
				&lt;ene&gt;0.60000000&lt;/ene&gt;
				&lt;ene&gt;0.80000000&lt;/ene&gt;
				&lt;ene&gt;1.00000000&lt;/ene&gt;
			&lt;/trial&gt;
			&lt;trial name="t04"&gt;
				&lt;dir&gt;mm~qmmm/aq/t04/dats&lt;/dir&gt;
				&lt;ene&gt;0.00000000&lt;/ene&gt;
				&lt;ene&gt;0.20000000&lt;/ene&gt;
				&lt;ene&gt;0.40000000&lt;/ene&gt;
				&lt;ene&gt;0.60000000&lt;/ene&gt;
				&lt;ene&gt;0.80000000&lt;/ene&gt;
				&lt;ene&gt;1.00000000&lt;/ene&gt;
			&lt;/trial&gt;
		&lt;/stage&gt;
	&lt;/env&gt;
	&lt;env name="reference"&gt;
		&lt;stage name="STAGE"&gt;
			&lt;trial name="t01"&gt;
				&lt;dir&gt;mm~qmmm/vac/t01/dats&lt;/dir&gt;
				&lt;ene&gt;0.00000000&lt;/ene&gt;
				&lt;ene&gt;0.20000000&lt;/ene&gt;
				&lt;ene&gt;0.40000000&lt;/ene&gt;
				&lt;ene&gt;0.60000000&lt;/ene&gt;
				&lt;ene&gt;0.80000000&lt;/ene&gt;
				&lt;ene&gt;1.00000000&lt;/ene&gt;
			&lt;/trial&gt;
			&lt;trial name="t02"&gt;
				&lt;dir&gt;mm~qmmm/vac/t02/dats&lt;/dir&gt;
				&lt;ene&gt;0.00000000&lt;/ene&gt;
				&lt;ene&gt;0.20000000&lt;/ene&gt;
				&lt;ene&gt;0.40000000&lt;/ene&gt;
				&lt;ene&gt;0.60000000&lt;/ene&gt;
				&lt;ene&gt;0.80000000&lt;/ene&gt;
				&lt;ene&gt;1.00000000&lt;/ene&gt;
			&lt;/trial&gt;
			&lt;trial name="t03"&gt;
				&lt;dir&gt;mm~qmmm/vac/t03/dats&lt;/dir&gt;
				&lt;ene&gt;0.00000000&lt;/ene&gt;
				&lt;ene&gt;0.20000000&lt;/ene&gt;
				&lt;ene&gt;0.40000000&lt;/ene&gt;
				&lt;ene&gt;0.60000000&lt;/ene&gt;
				&lt;ene&gt;0.80000000&lt;/ene&gt;
				&lt;ene&gt;1.00000000&lt;/ene&gt;
			&lt;/trial&gt;
			&lt;trial name="t04"&gt;
				&lt;dir&gt;mm~qmmm/vac/t04/dats&lt;/dir&gt;
				&lt;ene&gt;0.00000000&lt;/ene&gt;
				&lt;ene&gt;0.20000000&lt;/ene&gt;
				&lt;ene&gt;0.40000000&lt;/ene&gt;
				&lt;ene&gt;0.60000000&lt;/ene&gt;
				&lt;ene&gt;0.80000000&lt;/ene&gt;
				&lt;ene&gt;1.00000000&lt;/ene&gt;
			&lt;/trial&gt;
		&lt;/stage&gt;
	&lt;/env&gt;
&lt;/edge&gt;</code></pre>
	<div class="caption">
	  <span class="label">Figure 7</span>. The edgembar input file describing the
	  directory structure to locate the efep_*_*.dat and dvdl_*.dat files within
	  the <code>analysis/</code> directory.
	  The name of the edge is "mm~qmmm".
	  There are two environments: "target" is the solvated phase and "reference"
	  is the gas phase.
	  The free energy simulations involved a single stage, which we simply name "STAGE".
	</div>
      </div>
      To run edgembar, change directory to the analysis directory and run
      <code>edgembar --no-auto ./xml/mm~qmmm.xml</code>. The output will be written
      to <code>./xml/mm~qmmm.py</code>. (The output is a python script.)
      One can view the output as an HTML file by running <code>python3 ./xml/mm~qmmm.py</code>,
      which will produce <code>./xml/mm~qmmm.html</code>.
      <a href="mm~qmmm.html">One can view the free energy report here</a>.
      The reported free energy difference, &Delta;&Delta;G
      -- <i>technically, the
	calculated free energies are Helmholtz free energies because we performed
	the bookend simulations at constant volume, but edgembar doesn't know that </i> --
      is the bookend free energy that one would use to correct the MM-calculated solvation
      free energy.
      <a href="https://rutgerslbsr.gitlab.io/fe-toolkit/edgembar/edgeana.html">A
	guide for understanding the free energy report</a>
      is provided by the
      <a href="https://rutgerslbsr.gitlab.io/fe-toolkit/edgembar/index.html">edgembar
	online documentation</a>.
      
    </p>
    
    
    
  </body>
</html>
