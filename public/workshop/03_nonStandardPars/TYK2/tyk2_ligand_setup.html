<html>
  <head>
    <title>Building and Simulating TYK2 protein Ligand System</title>
    <link href="tutorial.css" rel="stylesheet">
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script async="async" id="MathJax-script" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
  </head>
  <body>
    <h1>Building and Simulating TYK2 Protein Ligand System</h1>
    <div class="author">
      <p>Ryan Snyder¹ and Darrin M. York¹</p>
      <p>¹ Laboratory for Biomolecular Simulation Research, Institute for Quantitative Biomedicine and Department of Chemistry and Chemical Biology, Rutgers University, Piscataway, NJ 08854, USA</p>
    </div>
    <h2>Learning Objectives</h2>
    <ul>
      <li>Prepare a PDB to be AMBER compatible</li>
      <li>Find atom chargers and force-field parameters using antechamber</li>
      <li>Generate the files needed for simulations using tleap</li>
    </ul>
    <h2>Introduction</h2>
    <p>This tutorial will demonstrate how to prepare a set of ligand-receptor complexes with coordinates in PDB format for simulation in AMBER using AMBERTools.</p>
    <p>For this tutorial, we will prepare a set of ligands in complex with tyrosine kinase 2 (TYK2) for simulations in both the aqueous and ligand-receptor complex phases. The success of binding free energy calculations can be dependent on the initial pose of ligands in the receptor environment. As many ligands lack crystallographic data, there are two strategies commonly used to generate the initial structures [
      <a href=" https://doi.org/10.1007/s10822-017-0075-9">Athanasiou 2017</a>]. The first involves using a docking software, such as AutoDock Vina [
      <a href="https://pubs.acs.org/doi/10.1021/acs.jcim.1c00203">Eberhardt 2021</a>],  to generate an initial binding pose by docking the ligand into the receptor.  The second involves mapping the ligands onto a chemically simular ligand for which there is an existing structure for the ligand-receptor complex. For this case, we will use the latter method. In  this tutorial, we will prepare four ligands to be used in RBFE simulation a future tutorial. We will use ligands 31 and 42 from the Eur. J. Med. Chem article [
      <a href="https://doi.org/10.1016/j.ejmech.2013.03.070">Liang 2013 1</a>] hereon refered to as ejm_31 and ejm 42.  We will aslo use lignads 27 and 28 from the J. Med. Chem. article [
      <a href="https://pubs.acs.org/doi/10.1021/jm400266t">Liang 2013 2</a>], hereon refered to as jmc_27 and jmc_28. None of the four ligands (ejm_42, ejm_31, jmc_27, and jmc_28) have been crystalized in complex with TYK2; however, ligand 46 from the Eur. J. Med. Chem article is strucutally simular to the ligands to be used in this tutorial and does have crystallographic data (PDB: 4GIH). 
    </p>
    <h2>Initial Setup</h2>
    <p>To begin, let us prepare some directories. First, with-in the SmallMolecules directory prepared in the Acetic Acid tutorial, create a second directory called TYK2.</p>
    <h3>pdb4amber</h3>
    <p>Inside the TYK2 directory, download the pdb structure for PDB: 4GIH using pdb4amber : </p>
    <div class="file">
      <code>pdb4amber --pdbid 4GIH --dry --reduce -o 4gih.pdb</code>
    </div>
    <p>The --reduce option adds hydrogens to the protein, including residues that can have multiple protenation states. Please note that this may not be the best option for determining protenation state. More rigourous methods, such as using the H++ server may be advisable for applications outside of this tutorial.</p>
    <p>We will rename the small molecule that we will use for atom mapping : </p>
    <div class="file">
      <code>sed -i &quot;s/0X5/LIG/g&quot; 4gih.pdb</code>
    </div>
    <p>For our own convienence, we will also extract just the receptor to a pdb file: </p>
    <div class="file">
      <code>sed -e &quot;/LIG/d&quot; 4gih.pdb &gt; receptor.pdb</code>
    </div>
    <p>Both receptor.pdb and the final version of 4gih.pdb are available in the /expanse/projects/qstore/amber_ws/tutorials/pdb4amber/outputs directory</p>
    <h3>rdkit</h3>
    <p>Next, we will will map our set of ligands onto the existing set of ligands. The sdf files for these ligands can be found 
      <a href="files/initial_structures.tar.gz">here</a>. the initial coordinate for each ligand come from the Open Force Field Inititive's protein-ligand-benchmark [
      <a href="https://doi.org/10.33011/livecoms.4.1.1497">Hahn 2022</a>]. For an example, we will preapre the coordinates for ejm_42. Create a directory called ligands but do not enter the directory. Open a python file, atom_map.py in a text editor and write the following :
    </p><br>
    <div class="file">
      <code>from rdkit import Chem</code><br>
      <code>from rdkit.Chem import AllChem</code><br>
      <code>from rdkit.Chem import rdFMCS</code><br>
      <code>import numpy as np</code><br><br>
      <code># Load the PDB file</code><br>
      <code>pdb1 = Chem.MolFromPDBFile(&quot;4gih.pdb&quot;, removeHs=True)</code><br>
      <code></code><br>
      <code># Load the molecules</code><br>
      <code>mol1 = Chem.SDMolSupplier(&quot;ejm_42.sdf&quot;)[0]</code><br>
      <code></code><br>
      <code># Extract the ligand from the PDB</code><br>
      <code>mol = Chem.MolFromPDBBlock(&quot;&quot;)</code><br>
      <code>for res in Chem.GetMolFrags(pdb1, asMols=True):</code><br>
      <code>&nbsp;&nbsp;&nbsp;&nbsp;if res.GetAtomWithIdx(0).GetPDBResidueInfo().GetResidueName() == &quot;LIG&quot;:</code><br>
      <code>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mol2 = res</code><br>
      <code>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;break</code><br>
      <code></code><br>
      <code># Find the Maximum Common Substructure (MCS)</code><br>
      <code>mcs = rdFMCS.FindMCS([mol1, mol2])</code><br>
      <code>common_smarts = mcs.smartsString</code><br>
      <code>common_mol = Chem.MolFromSmarts(common_smarts)</code><br>
      <code></code><br>
      <code># Get the atom indices of the common scaffold in both molecules</code><br>
      <code>match1 = mol1.GetSubstructMatch(common_mol)</code><br>
      <code>match2 = mol2.GetSubstructMatch(common_mol)</code><br>
      <code></code><br>
      <code># Align mol1 to mol2 based on the common scaffold</code><br>
      <code>AllChem.AlignMol(mol1, mol2, atomMap=list(zip(match1, match2)))</code><br>
      <code># Add Hydrogens</code><br>
      <code>mol1 = Chem.AddHs(mol1, addCoords=True)</code><br><br>
      <code># Save the aligned molecule to a new PDB file</code><br>
      <code>Chem.MolToPDBFile(mol1, &quot;ligands/ejm_42.pdb&quot;)</code><br>
    </div>
    <p>The used python to run this script: <br></p>
    <div class="file">
      <code>python3 atom_map.py</code>
    </div>
    <p>The aligned coordinates for ejm_42 will be written to ligands/ejm_42.pdb. This file is also available at /expanse/projects/qstore/amber_ws/tutorials/Param_tyk2/pdb4amber/outputs/ejm_42.pdb. Load both ligands/ejm_42.pdb and 4gih.pdb into VMD. As an exersize, verify that the two ligands (both using RESNAME LIG) are aligned (Fig. 1).</p>
    <div style="text-align: center;">
      <figure>
        <img alt="Figure 1" src="figures/Fig1_tyk2_setup.png">
        <figcaption>
          <b>Figure 1. </b>EJM Ligands 42 (Green) and 46 (Blue) are aligned.
        </figcaption>
      </figure>
    </div>
    <h3>antechamber</h3>
    <p>Before we can continue, we will need to find the atom charges and atom-types to be used in our simulations. To do this we will follow a procedure simular to that previously used in the acetic acid tutorial : </p>
    <p>First, we will assign atomic charges and atom types and ouput the new charges, atom types, and coordinates to a mol2. The atom types will be assigned using the GAFF2 forcefield which is parameterized for RESP charges; consequently, we will assign chargers using the RESP method, which requires the Gaussian QM software package. We will do this in two steps. First, we will generate the inputfiles needed for Gaussian using antechamber:</p>
    <div class="file">
      <code>antechamber -i ligands/ejm_42.pdb -fi pdb -o ejm_42.g16 -fo gcrt -nc 0 -gk 'p HF/6-31G(d) nosymm Pop=MK  iop(6/33=2) iop(6/42=6) iop(6/50=1)' -gv 1 -ge ejm_42.resp -gn '%nproc=1' -gm '%mem=8000MB'</code>
    </div>
    <p>Now that we have the Gaussian files needed to calculate the RESP charges, load the Gaussian module, define the Gaussian scratch directory and pass the g16 file to Gaussian :</p>
    <div class="file">
      <code>module purge</code><br>
      <code>module load cpu/0.15.4</code><br>
      <code>module load gaussian/16.C.01</code><br>

      <code>GAUSS_SCRDIR=/scratch/$USER/job_$SLURM_JOBID</code><br>
      <code>g16 ejm_42.g16 &gt; ejm_42.log</code>
    </div>
    <p>If Gaussian is unavailable, the log file is available at /expanse/projects/qstore/amber_ws/tutorials/Param_tyk2/antechamber/intermediates_io.</p>
    <p>Once Gaussian has completed the calculation, reload the amber modules and use antechamber to extract the charges from the log file and assign atom types: </p>
    <div class="file">
      <code>module load workshop/software<br>antechamber -i ejm_42.log -fi gout -gv 0 -o ejm_42.mol2 -fo mol2 -c resp -nc 0 -rn LIG -at gaff2</code>
    </div>
    <p>The mol2 file is available in the /expanse/projects/qstore/amber_ws/tutorials/Param_tyk2/antechamber/outputs.</p>
    <h3>parmchk2</h3>
    <p>Now that we have the mol2 file, we can generate the frcmod file using the parmchk2 executable:</p>
    <div class="file">
      <code>parmchk2 -i ejm_42.mol2 -o ejm_42.frcmod -f mol2 -s 2</code>
    </div>
    <p>Like antechamber, you can learn more about the executable options for parmchk2 using &quot;parmchk2 -h&quot;. The ejm_42.frcmod file is available at /expanse/projects/qstore/amber_ws/tutorials/Param_tyk2/parmchk2/outputs</p>
    <h3>tleap</h3>
    <p>Next, we will use the mol2 and frcmod files to generate our AMBER formated parameters and restart files. We will solvate the sytem using the OPC water model. Open a file ejm_42_aq_tleap.in in a text editor and write the following : </p>
    <div class="file">
      <code>source leaprc.protein.ff19SB<br>loadamberparams frcmod.ff19SB<br>source leaprc.water.opc<br>loadamberparams frcmod.opc<br>source leaprc.gaff2<br>loadamberparams ejm_42.frcmod<br><br>LIG = loadmol2 &quot;ejm_42.mol2&quot;<br>saveOff LIG ejm_42.lib<br>solvateOct ejm_42 OPCBOX 20.0<br>saveAmberParm ejm_42 ejm_42_aq.parm7 ejm_42_aq.rst7<br><br>quit</code>
    </div>
    <p>Then run the command:</p>
    <div class="file">
      <code>tleap -f ejm_42_aq_tleap.in</code>
    </div>
    <p>Next, we will prepare the protein ligand complex systems : </p><br>
    <div class="file">
      <code>source leaprc.protein.ff19SB<br>loadamberparams frcmod.ff19SB<br>source leaprc.water.opc<br>loadamberparams frcmod.opc<br>source leaprc.gaff2<br>loadamberparams ejm_42.frcmod<br><br>ejm_42_lig = loadmol2 &quot;ejm_42.mol2&quot;<br>ejm_42_rec = loadpdb &quot;receptor.pdb&quot;<br>complex = combine {ejm_42_lig ejm_42_rec}<br>savepdb complex ejm_42.pdb<br>addIons complex Na+ 0<br>solvateOct complex OPCBOX 16.0<br>saveAmberParm complex ejm_42_com.parm7 ejm_42_com.rst7<br><br>quit</code>
    </div>
    <p>Then run the command:</p>
    <div class="file">
      <code>tleap -f ejm_42_com_tleap.in</code>
    </div>
    <p>Because the protein carries a net charge of -2, sodium counter ions are added. The parm7, rst7 and lib files are available at /expanse/projects/qstore/amber_ws/tutorials/Param_tyk2/tleap/outputs</p>
    <p>Repeat this process for the remaining ligands. This can be automated by running the shell script available at /expanse/projects/qstore/amber_ws/tutorials/Param_tyk2/misc/run.sh</p>
    <h2>References</h2>
    <ul>
      <li>Using physics-based pose predictions and free energy perturbation calculations to predict binding poses and relative binding affinities for FXR ligands in the D3R Grand Challenge 2.<br>C. Athanasiou, S. Vasilakaki, D. Dellis, and Z. Cournia<br>J. Comput. Aided. Mol. Des. (2018) 32, 21-44 (
        <a href="https://doi.org/10.1007/s10822-017-0075-9">10.1007/s10822-017-0075-9</a>)
      </li>
      <li>AutoDock Vina 1.2.0: New Docking Methods, Expanded Force Field, and Python Bindings<br>J. Eberhardt, D. Santos-Martins, A. F. Tillack, and S. Forli<br>J. Chem. Inf. Model. (2021) 61, 3891–3898 (
        <a href="https://doi.org/10.1021/acs.jcim.1c00203">https://doi.org/10.1021/acs.jcim.1c00203</a>)
      </li>
      <li>Lead identification of novel and selective TYK2 inhibitors<br>J. Liang, V. Tsui, A. Van Abbema, L. Bao, K. Barrett, M. Beresis, L. Berezhkovskiy, W. S. Blair, C. Chang, J. Driscoll, C. Eigenbrot, N. Ghilardi, P. Gibbons, J. Halladay, A. Johnson, P. Bir Kohli, Y. Lai, M. Liimatta, P. Mantik, K. Menghrajani, J. Murray, A. Sambrone, Y. Xiao, S. Shia, Y. Shin, J. Smith, S. Sohn, M. Stanley, M. Ultsch, B. Zhang, L. C. Wu, S. Magnuson<br>Eur. J. Med. Chem. (2013) 67, 175-187 (
        <a href="http://dx.doi.org/10.1016/j.ejmech.2013.03.070">10.1016/j.ejmech.2013.03.070</a>)
      </li>
      <li>Lead Optimization of a 4-Aminopyridine Benzamide Scaffold To Identify Potent, Selective, and Orally Bioavailable TYK2 Inhibitors<br>J. Liang, A. Van Abbema, M. Balaz, K. Barrett, L. Berezhkovsky, W. Blair, C. Chang, D. Delarosa, J. DeVoss, J. Driscoll, C. Eigenbrot, N. Ghilardi, P. Gibbons, J. Halladay, A. Johnson, P. Bir Kohli, Y. Lai, Y. Liu, J. Lyssikatos, P. Mantik, K. Menghrajani, J. Murray, I. Peng, A. Sambrone, W. Yang, J. Young, B. Zhang, B. Zhu, S. Magnuson<br>J. Med. Chem. (2013) 56, 4521-4536 (
        <a href="https://doi.org/10.1021/jm400266t">10.1021/jm400266t</a>)
      </li>
      <li>Best Practices for Constructing, Preparing, and Evaluating Protein-Ligand Binding Affinity Benchmarks<br>D. F. Hahn, C. I. Bayly, M. L. Boby, H. E. Bruce Macdondald, J. D. Chodera, V. Gapsys, A. S. J. S. Mey, D. L. Mobley, L. Perez Benito, C. E. M. Schindler, G. Tresadern, and G. L. Warren<br>Living J. Comp. Mol. Sci. (2022) 4, 1497 (
        <a href="https://doi.org/10.33011/livecoms.4.1.1497">10.33011/livecoms.4.1.1497</a>)
      </li>
    </ul>
  </body>
</html>
