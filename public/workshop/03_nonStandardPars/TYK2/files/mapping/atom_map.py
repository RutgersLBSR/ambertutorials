from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit.Chem import rdFMCS
import numpy as np

# Load the PDB file
pdb1 = Chem.MolFromPDBFile("4gih.pdb", removeHs=True)

# Load the molecules
mol1 = Chem.SDMolSupplier("MOL.sdf")[0]

# Extract the ligand from the PDB
mol = Chem.MolFromPDBBlock("")
for res in Chem.GetMolFrags(pdb1, asMols=True):
    if res.GetAtomWithIdx(0).GetPDBResidueInfo().GetResidueName() == "LIG":
       mol2 = res
       break

# Find the Maximum Common Substructure (MCS)
mcs = rdFMCS.FindMCS([mol1, mol2])
common_smarts = mcs.smartsString
common_mol = Chem.MolFromSmarts(common_smarts)

# Get the atom indices of the common scaffold in both molecules
match1 = mol1.GetSubstructMatch(common_mol)
match2 = mol2.GetSubstructMatch(common_mol)

# Align mol1 to mol2 based on the common scaffold
AllChem.AlignMol(mol1, mol2, atomMap=list(zip(match1, match2)))

# Add Hydrogens
mol1 = Chem.AddHs(mol1, addCoords=True)

# Save the aligned molecule to a new PDB file
Chem.MolToPDBFile(mol1, "ligands/MOL.pdb")
