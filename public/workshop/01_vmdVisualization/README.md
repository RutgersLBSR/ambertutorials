Definition: AllSys will be used to mean classical MD of all the systems: 
-acetic acid in gas phase
-acetic acid solution
-wild type MTR1 with native ligand and C10 protonated
-Protein-ligand complex end states (4 ligands)

Task: Using VMD to visualize structures and trajectories and analysis with cpptraj
-Here the students will learn the basics of vmd and visualizing trajectories and doing minimal property measurements using vmd, followed by using cpptraj to carry out trajectory-wide analyses. 
-Software/code: vmd, cpptraj, xmgrace 
-Calculations run by students: none 
-Data provided by us: AllSys trajectories

