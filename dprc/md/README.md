# Run NVT MD using DPRc models

Assume you have trained 4 models, i.e., [graph.000.pb](graph.000.pb), [graph.001.pb](graph.001.pb), [graph.002.pb](graph.002.pb), [graph.003.pb](graph.003.pb).
This tutorial will run NVT MD simulations for 200 steps using [graph.000.pb](graph.000.pb), and check the maximum force standard deviation among these four models every 10 steps.

Running
```sh
sander -O -p ETP_ETH.parm7 -c init.rst7 -i init0.mdin -o rc.mdout -r rc.rst7 -frc rc.mdfrc -inf rc.mdinfo
```

The output file looks like

```txt
===============================================================================
Active learning frame written with max. frc. std.:     1.73676 kcal/mol/A
wrapping first mol.:      -16.34305       46.22512        0.00000

 NSTEP =      190   TIME(PS) =     238.190  TEMP(K) =   305.62  PRESS =     0.0
 Etot   =   -551994.0733  EKtot   =      5433.0467  EPtot      =   -557427.1201
 BOND   =         0.0000  ANGLE   =         0.0000  DIHED      =         0.0000
 1-4 NB =         0.0000  1-4 EEL =         0.0000  VDWAALS    =      6189.0331
 EELEC  =    -38966.0318  EHBOND  =         0.0000  RESTRAINT  =         0.9364
 MNDODESCF=     -688.6437
 ML =  -523962.4141
 EAMBER (non-restraint)  =   -557428.0565
 Ewald error estimate:   0.2154E-01
 ------------------------------------------------------------------------------

 NMR restraints: Bond =    0.000   Angle =     0.749   Torsion =     0.000
               : Gen. Dist. Coord. =     0.188
===============================================================================
Active learning frame written with max. frc. std.:     1.68062 kcal/mol/A
wrapping first mol.:      -16.34305       46.22512        0.00000
wrapping first mol.:      -16.34305       46.22512        0.00000

 NSTEP =      200   TIME(PS) =     238.200  TEMP(K) =   300.65  PRESS =     0.0
 Etot   =   -552055.5489  EKtot   =      5344.8161  EPtot      =   -557400.3649
 BOND   =         0.0000  ANGLE   =         0.0000  DIHED      =         0.0000
 1-4 NB =         0.0000  1-4 EEL =         0.0000  VDWAALS    =      6231.2573
 EELEC  =    -38982.2872  EHBOND  =         0.0000  RESTRAINT  =         0.5120
 MNDODESCF=     -680.1063
 ML =  -523969.7407
 EAMBER (non-restraint)  =   -557400.8769
 Ewald error estimate:   0.2117E-01
 ------------------------------------------------------------------------------

 NMR restraints: Bond =    0.000   Angle =     0.512   Torsion =     0.000
               : Gen. Dist. Coord. =     0.000
===============================================================================
```


